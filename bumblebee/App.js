import React, { useState } from 'react';
import fire from './config';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {Image } from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';

import LoadingScreen  from './components/screens/LoadingScreen';
import LoginScreen from './components/screens/LoginScreen';
import RegisterScreen from './components/screens/RegisterScreen';

import HomeScreen from './components/screens/HomeScreen';
import ProblemsScreen from './components/screens/ProblemsScreen';
import AddProblemScreen from './components/screens/AddProblemScreen';
import ProfileScreen from './components/screens/ProfileScreen';


/*const firebaseConfig = {
    apiKey: "AIzaSyA1HVDEr7a_ejUEGbbFdFLBD_Yr3ENuyNM",
    authDomain: "next-heroes.firebaseapp.com",
    databaseURL:"https://next-heroes.firebaseio.com",
    projectId: "next-heroes",
    storageBucket: "next-heroes.appspot.com",
    messagingSenderId: "999160660233",
    appId: "1:999160660233:web:fad941f8912ae383ef93d8",
    measurementId: "G-1Y9Q1W34V8"
  };
  try{
     firebase.initializeApp(firebaseConfig);
  }catch(error ){
    
  }*/
  const AppContainer = createStackNavigator(
    {
      default: createBottomTabNavigator (
        {
          AddProblem: {
            screen: AddProblemScreen,
            navigationOptions: {
              tabBarIcon: <Image style={{width: 25, height:25}} source={require('./assets/plusnavigationicon.png')}></Image>
            }
          },
          Problems: {
            screen: HomeScreen,
            navigationOptions: {
              tabBarIcon: <Image style={{width: 25, height:25}} source={require('./assets/navigationmonstericon.png')}></Image>
            }
          },
          Profile: {
            screen: ProfileScreen,
            navigationOptions: {
              tabBarIcon: <Image style={{width: 25, height:25}} source={require('./assets/navigationprofileicon.png')}></Image>
            }
          }
      },
      {
        defaultNavigationOptions:{
          tabBarOnPress:({navigation, defaultHandler}) =>{
              if(navigation.state.key==='AddProblem'){
                navigation.navigate('postModal')
              } else {
                defaultHandler()
              }
          }
        },
        tabBarOptions: {
       activeTintColor:'red',
       inactiveTintColor: 'grey',
      }
    
    }
   ),
    postModal: {
      screen: AddProblemScreen
    }
  },
  {
    mode: 'modal',
    headerMode: 'none'
  }
)

  const AuthStack = createStackNavigator ({
    Login: LoginScreen,
    Register: RegisterScreen
  });

  export default createAppContainer(
    createSwitchNavigator(
      {
        Loading: LoadingScreen,
        App: AppContainer,
        Auth: AuthStack
      }
      ,{
        initialRouteName:"Loading",  
        defaultNavigationOptions: {
          header: null
        },   
      },
    )
  );
 
  