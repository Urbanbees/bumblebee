import *  as firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyA1HVDEr7a_ejUEGbbFdFLBD_Yr3ENuyNM",
    authDomain: "next-heroes.firebaseapp.com",
    databaseURL:"https://next-heroes.firebaseio.com",
    projectId: "next-heroes",
    storageBucket: "next-heroes.appspot.com",
    messagingSenderId: "999160660233",
    appId: "1:999160660233:web:fad941f8912ae383ef93d8",
    measurementId: "G-1Y9Q1W34V8"
  };
   var fire=  firebase.initializeApp(firebaseConfig);
export default fire;