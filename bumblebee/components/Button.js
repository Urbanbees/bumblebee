import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, Image } from 'react-native';

const Button = ({onPress,colorbutton, children}) => {
    if(colorbutton=="green"){
        return(
            <TouchableOpacity onPress={onPress} style={styles.button}>
                <Image
                source= {require('../assets/buttons/buttongreen.png')} 
                resizeMode='contain' 
                style={{height:40}} 
                />  
                <Text style={styles.text}>{children}</Text>
            </TouchableOpacity>
        )
    }else if(colorbutton=="red"){
        return(
            <TouchableOpacity onPress={onPress} style={styles.button}>
                <Image
                source= {require('../assets/buttons/buttonred.png')} 
                resizeMode='contain' 
                style={{height:40}} 
                />  
                <Text style={styles.text}>{children}</Text>
            </TouchableOpacity>
        )
    }else{
        return(
            <TouchableOpacity onPress={onPress} style={styles.button}>
                <Image
                source= {require('../assets/buttons/buttonyellow.png')} 
                resizeMode='contain' 
                style={{height:40}} 
                />  
                <Text style={styles.text}>{children}</Text>
            </TouchableOpacity>
        )
    }
}

const styles= StyleSheet.create({
    button:{
        marginTop: 10,
        padding: 20,
        width: '100%',
        borderRadius: 4,
        alignItems: 'center',
    },
    text: {
        color: 'white',
        fontWeight: '700',
        fontSize: 18,
        marginTop:-35,
        color: "black"
    }
});

export { Button };