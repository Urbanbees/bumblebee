import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, StatusBar } from 'react-native';
import {Ionicons} from '@expo/vector-icons';
import { Input } from '../input';
import {Button} from '../Button';
import * as firebase from 'firebase';
import fire from '../../config';

export default class ProfileScreen extends React.Component {
  static navigationOptions = {
    headerShown: false
};
  state= {
    name:'Hero name ',
    coins: 0,
    problemssolved: 0,
    problemsproposed: 0
  }
  signOutUser = ()=> {
    firebase.auth().signOut();
};
componentDidMount() {
  var heroname= firebase.auth().currentUser.displayName;
  var heroid=firebase.auth().currentUser.uid;
  this.setState({name: heroname});
  

  fire.database().ref('user/'+heroid).once('value', snapshot =>{
    let herocoins= snapshot.child('coins').val();
    let heromonsters= snapshot.child('solutions').val();
    let heroproblemsposted= snapshot.child('problems').val();

    this.setState({coins: herocoins});
    this.setState({problemssolved: heromonsters});
    this.setState({problemsproposed: heroproblemsposted});
  }

  );

}
    render(){
      return(

        <View style={styles.container}>
                <StatusBar barStyle='light-content'></StatusBar>
                <Image 
                source ={require('../../assets/Hero1.gif')}
                resizeMode='contain' 
                style={styles.logostyle}
                >  
                </Image>
                <Text style={styles.greeting}>{this.state.name}</Text>
                 <View style={styles.stats}>  
                    <View style={styles.together}>
                        <Image 
                        source ={require('../../assets/coin.png')}
                        resizeMode='contain' 
                        style={styles.staticon}
                        >   
                        </Image>
                        <Text style={styles.textforicons2} >
                          {this.state.coins}
                        </Text> 
                        <Text style={styles.textforicons} >
                          TOTAL COINS
                        </Text>     
                    </View>  
                    <View style={styles.together}>       
                        <Image 
                        source ={require('../../assets/bountybag.png')}
                        resizeMode='contain' 
                        style={styles.staticon}
                        >         
                       </Image>
                       <Text style={styles.textforicons2} >
                          {this.state.problemssolved}
                        </Text> 
                        <Text style={styles.textforicons} >
                            MONSTERS SLAYED
                        </Text>  
                    </View> 
                    <View style={styles.together}> 
                        <Image 
                        source ={require('../../assets/slimes/slimelvl1.png')}
                        resizeMode='contain' 
                        style={styles.staticon2}
                        >  
                        </Image>
                        <Text style={styles.textforicons2} >
                          {this.state.problemsproposed}
                        </Text> 
                        <Text style={styles.textforicons} >
                            PROBLEMS POSTED
                        </Text>
                    </View> 
                 </View>
                <View style={styles.form}>
                    <View style={styles.margins}>
                        <Button onPress={this.signOutUser } colorbutton="red" >Logout</Button>
                    </View>
                </View>
            </View>
      );
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#8C52FF"
  },
  greeting: {
      marginTop: 15,
      fontSize: 30,
      fontWeight: "700",
      textAlign: "center",
      color: 'white'
  },
  errorMessage: {
      height:72,
      justifyContent: "center",
      alignItems: "center",
      marginHorizontal: 30
  },
  signup:{
      alignSelf:"center",
      marginTop: 32
  },
  error:{
      color: "red",
      fontSize: 13,
      fontWeight: "600", 
      textAlign: "center"

  },
  logostyle: {
      marginTop: "20%",
      width: "70%",
      height:120,
      alignSelf:"center",
  },
  margins:{
   marginHorizontal: "10%",
  },
  back: {
      position: 'absolute',
      top: 18,
      left: 22,
      width: 32,
      height: 32,
      borderRadius: 16,
      backgroundColor: 'rgba(21,22,48,0.3)',
      alignItems: 'center',
      justifyContent: 'center'
  },
  stats:{
   
  },
  staticon:{
    width: 200,
    height:100,
  },
  together:{ 
    flexDirection:'row'
  },
  textforicons: {
    marginTop:55,
    paddingRight:5,
    fontWeight: "700",
    color: "white",
    fontSize: 13,
  },
  textforicons2: {
    marginTop:20,
    marginLeft:-45,
    paddingRight:5,
    fontWeight: "700",
    fontSize: 50,
    color: "white"
  },
  staticon2:{
    width: 200,
    height:100,
  },
  });