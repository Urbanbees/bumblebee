import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, LayoutAnimation,FlatList, Image   } from 'react-native';
import * as firebase from 'firebase';
import { Input } from '../input';
import {Button} from '../Button';
posts= [
  {
    id:"1",
    name: "Corona time",
    price:100,
  },
  {
    id:"2",
    name: "Lack of toilet paper in Berlin",
    price:200,
  },
  {
    id:"3",
    name: " Gamification to fight COVID19",
    price:300,
  },
  {
    id:"4",
    name: "Robery ",
    price:80,
  },
  {
    id:"5",
    name: "Sanitation",
    price:50,
  }
];

export default class HomeScreen extends React.Component {
    state ={
        email: '',
        displatName:''
    };
  
    componentDidMount(){
        const {email,displayName}= firebase.auth().currentUser;
        this.setState({email,displayName});

    }
    getItems(){
      let items= [{title: "primero"},{title: "segundo"}];
      
    }
    renderPost= post =>{
        return(
          <View style={styles.post}>
              <TouchableOpacity style={styles.organizeitems}>
                <Image 
                  source ={require('../../assets/slimes/slimelvl2.gif')}
                  resizeMode='contain' 
                  style={styles.imagestyle}
                ></Image>  
                <View >
                  <Text style={ styles.posttext}>{post.name}</Text>
                  <View style={styles.organizeitems}>
                    <Image 
                    source ={require('../../assets/bountybag.png')}
                    resizeMode='contain' 
                    style={styles.imagestyle}
                    ></Image>  
                    <Text style={ styles.postmoney}>{post.price} </Text>
                  </View>
                </View> 
              
              </TouchableOpacity>
          </View> 

        );
    };
       render(){
           LayoutAnimation.easeInEaseOut();
         return(
           <View style={styles.container}>
              <View style={styles.title}>
                <Text style={ styles.titletext}>Monsters</Text>
              </View> 
            <FlatList 
              style={styles.feed}
              data={posts}
              renderItem={({item})=>this.renderPost(item)}
              keyExtractor={item=>item.id}
              showsVerticalScrollIndicator={false}
              />
            </View>

         );
       }
      }
     
     
     const styles = StyleSheet.create({
       container: {
         flex: 1,
         paddingHorizontal: 20,
         alignItems: 'center',
         backgroundColor: "#8C52FF"
       },
       title:{
         margin: 40,  
         marginBottom:10, 
         borderColor:"black",
         borderWidth:2, 
         borderRadius:18,
         justifyContent: "center",
         backgroundColor: "white",
       },
       titletext:{
          marginHorizontal: 70,
          marginVertical: 10,
          fontSize:30,
          fontWeight: "700"
       },
       post:{
        marginVertical: 10,   
        marginHorizontal:20,
        borderColor:"black",
        borderWidth:2, 
        borderRadius:18,
        backgroundColor: "white",
        
       },
       posttext:{
        fontSize:20,
        fontWeight: "700",
        maxWidth: 180,
       },
       imagestyle:{
        width: "50%",
        height:90,
        marginVertical:10,
        marginLeft:-30,
        
       },
       organizeitems:{
        flexDirection: "row",
       },
       postmoney:{
        fontSize:20,
        fontWeight: "700",
        maxWidth: 180,
        marginTop:45,
        marginLeft:10,
       }
     });