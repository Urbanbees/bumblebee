import React, { useState } from 'react';
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity,Image,TextInput } from 'react-native';
import {Ionicons} from '@expo/vector-icons';
import { Input } from '../input';
import {Button} from '../Button';
import * as firebase from 'firebase';
import fire from '../../config';
import { getAutoFocusEnabled } from 'expo/build/AR';
export default class AddProblemsScreen extends React.Component {
  state= {
    name:'',
    description: '',
    errorMessage: null,
    title:'Add a Problem',
    succesMessage: null
  }
  handleupload =()=>{
    var uid= firebase.auth().currentUser.uid;
    if(this.state.name==''|| this.state.description=='' ){
      this.setState({errorMessage: "Cant add empty values"});
    }else{
      fire.database().ref('/problems/').push({
        name: this.state.name,
        description:this.state.description,
        author: uid,
        bounty: 0,
        deadline: "22.03.2020"
        
    }) 
    this.setState({succesMessage: "Problem added successfully"});
    this.setState({name:''});
    this.setState({description: ''});
    }

    


 };
    render(){
      return(
        <View style={styles.container}>
          <TouchableOpacity  style={styles.back} onPress={() => this.props.navigation.goBack()}>
            <Ionicons name='ios-arrow-round-back' size={32} color='black'></Ionicons>
          </TouchableOpacity>

          <Image 
                source ={require('../../assets/slimes/slimelvl1.gif')}
                resizeMode='contain' 
                style={styles.logostyle}
                >  
                </Image>
                <Text style={styles.greeting}>{this.state.title}</Text>

                <View style={styles.errorMessage}>
                    {this.state.errorMessage && <Text style={styles.error}>{this.state.errorMessage}</Text>}
                    {this.state.succesMessage && <Text style={styles.success}>{this.state.succesMessage}</Text>}
                </View>            
                <View style={styles.form}>
                    <View style={styles.margins}>
                        <Input
                            placeholder= 'Name'
                            label='Name'
                            onChangeText= {name => this.setState({ name })}
                            value={this.state.name}
                        />
                        <Input
                            placeholder= ''
                            label='Description'
                            onChangeText= {description => this.setState({ description })}
                            value={this.state.description}
                            multiline={true}
                            numberOfLines={5}  
                        />
                        <Button onPress={() => this.handleupload() } colorbutton="red" >Add Problem</Button>
                    </View>
                </View>

        </View>
      );
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#8C52FF"

    },
    back: {
        position: 'absolute',
        top: 18,
        left: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.3)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    logostyle: {
        marginTop: "30%",
        width: "50%",
        height:100,
        alignSelf:"center",
    },
    greeting: {
        marginTop: 32,
        fontSize: 18,
        fontWeight: "700",
        textAlign: "center"
    },
    errorMessage: {
      height:72,
      justifyContent: "center",
      alignItems: "center",
      marginHorizontal: 30,
  },
    margins:{
     marginHorizontal: "10%",
    },
    signup:{
      alignSelf:"center",
      marginTop: 32
  },
  descriptionstyle:{
    borderEndColor:'black',
    borderBottomWidth: 1,
  },
   error:{
    color: "red",
    fontSize: 13,
    fontWeight: "600", 
    textAlign: "center"

},
success:{
  color: "green",
  fontSize: 16,
  fontWeight: "700", 
  textAlign: "center"

},
  });

