import React, { useState } from 'react';
import { StyleSheet, Text, View, ActivityIndicator, Image } from 'react-native';
import * as firebase from 'firebase';

export default class LoadingScreen extends React.Component {
    componentDidMount() {
        firebase.auth().onAuthStateChanged(user =>{
            this.props.navigation.navigate(user ? "App":"Auth");
        })
    }
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.margins}>
                    <Image 
                        source ={require('../../assets/logo.png')}
                        resizeMode='contain' 
                        style={styles.logostyle}
                    >  
                    </Image>
                    <Text style={styles.greeting}>{'Next Heroes'}</Text>
                    <ActivityIndicator size="large"  color="#000000"></ActivityIndicator>
                </View>
            </View>
        );
    }  

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#8C52FF",
    },
     greeting: {
        fontSize: 18,
        fontWeight: "400",
        textAlign: "center"
    },
    logostyle: {
        marginTop: "50%",
        width: "50%",
        height:100,
        alignSelf:"center",
    },
    margins:{
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: "10%",
    }
});