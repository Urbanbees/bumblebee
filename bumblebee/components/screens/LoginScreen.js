import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, StatusBar, LayoutAnimation, Dimensions } from 'react-native';
import {Ionicons} from '@expo/vector-icons';
import { Input } from '../input';
import {Button} from '../Button';
import * as firebase from 'firebase';
const { width } = Dimensions.get('window');
export default class LoginScreen extends React.Component {
    static navigationOptions = {
        headerShown: false
    };
    state= {
        email: '',
        password: '',
        authenticating: false,
        errorMessage: null
      }
      handleLogin =()=>{
          const {email, password}= this.state;
          firebase
            .auth()
            .signInWithEmailAndPassword(email,password)
            .catch(error=> this.setState({errorMessage: error.message}));
      };
    render(){
        LayoutAnimation.easeInEaseOut();
        return(
            <View style={styles.container}>
                <StatusBar barStyle='light-content'></StatusBar>
                <TouchableOpacity  style={styles.back} onPress={() => this.props.navigation.goBack()}>
                    <Ionicons name='ios-arrow-round-back' size={32} color='black'></Ionicons>
                </TouchableOpacity>
                <Image 
                source ={require('../../assets/logo.png')}
                resizeMode='contain' 
                style={styles.logostyle}
                >  
                </Image>
                <Text style={styles.greeting}>{'Next Heroes'}</Text>
                <View style={styles.errorMessage}>
                    {this.state.errorMessage && <Text style={styles.error}>{this.state.errorMessage}</Text>}
                </View>

                <View style={styles.form}>
                    <View style={styles.margins}>
                        <Input
                            placeholder= 'Enter your email..'
                            label='Email'
                            onChangeText= {email => this.setState({ email })}
                            value={this.state.email}
                        />
                        <Input
                            placeholder= 'Enter your password..'
                            label='Password'
                            secureTextEntry
                            onChangeText= {password => this.setState({ password })}
                            value={this.state.password}
                        />
                        <Button onPress={() => this.handleLogin()} colorbutton="green">Log In</Button>

                        <TouchableOpacity 
                            style={styles.signup} 
                            onPress={()=>this.props.navigation.navigate('Register')}
                        >
                            <Text style={{color: "black",fontSize: 13}}>
                                New to Next Hero? <Text style={{fontWeight:"500", color:"red"}}>Sign Up</Text>
                            </Text>
                        </TouchableOpacity>

                    </View>
                </View>
            </View>
        );
    }  

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#8C52FF",

    },
    greeting: {
       
        fontSize: 18,
        fontWeight: "700",
        textAlign: "center",
    },
    errorMessage: {
        height:72,
        justifyContent: "center",
        alignItems: "center",
        marginHorizontal: 30
    },
    signup:{
        alignSelf:"center",
        marginTop: 32
    },
    error:{
        color: "red",
        fontSize: 13,
        fontWeight: "600", 
        textAlign: "center"

    },
    logostyle: {
        marginTop: "30%",
        width: "50%",
        height:100,
        alignSelf:"center",
    },
    margins:{
     marginHorizontal: "10%",
    },
    back: {
        position: 'absolute',
        top: 18,
        left: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.3)',
        alignItems: 'center',
        justifyContent: 'center'
    }
});