import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, StatusBar } from 'react-native';
import {Ionicons} from '@expo/vector-icons';
import { Input } from '../input';
import {Button} from '../Button';
import * as firebase from 'firebase';
import fire from '../../config';
export default class RegisterScreen extends React.Component {
    static navigationOptions = {
        headerShown: false
    };
    state= {
        name:'',
        email: '',
        password: '',
        authenticating: false,
        errorMessage: null
      }
      handleSignup =()=>{
        console.log("now");
         firebase
            .auth()
            .createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then(userCredentials =>{    
                fire.database().ref('/user/'+userCredentials.user.uid).set({
                    name: this.state.name,
                    email:this.state.email,
                    coins: 100,
                    problems: 0,
                    solutions: 0
                })  
               return userCredentials.user.updateProfile({
                    displayName: this.state.name
                });
            })
            .catch(error =>this.setState({errorMessage: error.message}));
      };
    render(){
        return(
            <View style={styles.container}>
                <StatusBar barStyle='light-content'></StatusBar>
                <TouchableOpacity  style={styles.back} onPress={() => this.props.navigation.goBack()}>
                    <Ionicons name='ios-arrow-round-back' size={32} color='black'></Ionicons>
                </TouchableOpacity>

                <Image 
                source ={require('../../assets/Hero1.gif')}
                resizeMode='contain' 
                style={styles.logostyle}
                >  
                </Image>
                <Text style={styles.greeting}>{'Sign up and get started!'}</Text>

                <View style={styles.errorMessage}>
                    {this.state.errorMessage && <Text style={styles.error}>{this.state.errorMessage}</Text>}
                </View>

                <View style={styles.form}>
                    <View style={styles.margins}>
                        <Input
                            placeholder= 'Name'
                            label='Name'
                            onChangeText= {name => this.setState({ name })}
                            value={this.state.name}
                        />
                        <Input
                            placeholder= 'Enter your email..'
                            label='Email'
                            onChangeText= {email => this.setState({ email })}
                            value={this.state.email}
                        />
                        <Input
                            placeholder= 'Enter your password..'
                            label='Password'
                            secureTextEntry
                            onChangeText= {password => this.setState({ password })}
                            value={this.state.password}
                        />
                        <Button onPress={() => this.handleSignup() } colorbutton="red" >Sign up</Button>

                        <TouchableOpacity
                         style={styles.signup}
                          onPress={() => this.props.navigation.navigate('Login')}>
                            <Text style={{color: "black",fontSize: 13}}>
                                Already a Hero? <Text style={{fontWeight:"500", color:"red"}}>Login</Text>
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }  

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#8C52FF"
    },
    greeting: {
        marginTop: 32,
        fontSize: 18,
        fontWeight: "400",
        textAlign: "center"
    },
    errorMessage: {
        height:72,
        justifyContent: "center",
        alignItems: "center",
        marginHorizontal: 30
    },
    signup:{
        alignSelf:"center",
        marginTop: 32
    },
    error:{
        color: "red",
        fontSize: 13,
        fontWeight: "600", 
        textAlign: "center"

    },
    logostyle: {
        marginTop: "30%",
        width: "50%",
        height:100,
        alignSelf:"center",
    },
    margins:{
     marginHorizontal: "10%",
    },
    back: {
        position: 'absolute',
        top: 18,
        left: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.3)',
        alignItems: 'center',
        justifyContent: 'center'
    }
});